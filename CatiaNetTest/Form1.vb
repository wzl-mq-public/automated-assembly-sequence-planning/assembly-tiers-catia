﻿Imports INFITF
Imports MECMOD
Imports NavigatorTypeLib
Imports ProductStructureTypeLib
Imports SPATypeLib




Public Class Form1

    Dim myCATIA As INFITF.Application

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try
            myCATIA = GetObject(, "CATIA.Application")
        Catch ex As Exception
            myCATIA = CreateObject("CATIA.Application")
        End Try

        myCATIA.Visible = True
        myCATIA.DisplayFileAlerts = True

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim myAssemblyTiers As AssemblyTiers2 = New AssemblyTiers2()

        Try
            myAssemblyTiers.ShowForm()
        Catch ex As Exception
            Debug.WriteLine(ex.ToString())
        End Try

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim hierarchicalAlgorithm As HierarchicalAssemblyTiers = New HierarchicalAssemblyTiers()

        Try
            hierarchicalAlgorithm.CatMain()
        Catch ex As Exception
            Debug.WriteLine(ex.ToString())
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        Dim andOrAlgorithm As AndOrDataExtraction = New AndOrDataExtraction()

        Try
            andOrAlgorithm.CatMain()
        Catch ex As Exception
            Debug.WriteLine(ex.ToString())
        End Try
    End Sub

End Class
