# Assembly Tiers Method

## Description

This project allows to create precedence graphs and AND/OR graphs from CAD assemblies with Catia v5. An assembly-by-disassembly algorithm, namely the assembly tiers method, is implemented in VB.NET. The assembly tiers method was initially described by Pintzos [link to paper](https://www.tandfonline.com/action/showCitFormats?doi=10.1080/0951192X.2015.1130260).

## Installation

- As a main CAD software, make sure to install *CATIA V5-6R2022*. (Other versions of V5 might work as well).
- In CATIA, make sure to enable part's axis systems: Tools > Options > Infrastructure > Part Infrastructure > Create an axis system
- Install *Visual Studio 2022* as IDE vor VB.NET.

## How to use the project

Follow the instruction in [User_Manual.pdf](User_Manual.pdf).

## Acknowledgements

This work is part of the research project “Internet of Construction” that is funded by the Federal Ministry of Education and Research of Germany within the indirective on a joint funding initiative in the field of innovation for production, services and labor of tomorrow (funding number: 02P17D081) and supported by the project management agency “Projekttr¨ager Karlsruhe (PTKA)”. The authors are responsible for the content.

The research was executed at [WZL of RWTH Aachen](https://www.wzl.rwth-aachen.de).
Main authors of the code: Mikhail Polikarpov & Sören Münker.